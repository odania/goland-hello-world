# Deploying Go Application with AWS ECS, CloudFormation, and an Application Load Balancer

This code repo provides a set of YAML templates for deploying a Golang app to production using Infrastructure as code approach . Cloudformation nested stack was used to build this infrastructure.


### infrastructure CloudFormation


| Template | Order of Execution | Description |
| -------- | :-----------------: | ----------- | 
| `vpc.yaml` | 0 |  Template to create VPC |
| `security-groups.yaml` | 1 |  Template to deploy security groups. |
| `load-balancers.yaml` | 2 | Template to create application load balancer | 
| `ecs-cluster-yaml` | 3 | Template to create ecs cluster | 
| `lifecyclehook.yaml` | 4 | Template to deploy lambda function to drain tasks from instances |
| `service.yaml` | 5 |  Template to setup instances and auto scaling group |
| `master.yaml` | 6 | Entry template to created all nested stacks|


### Network Architecture

![architecture-overview](http-echo-network.png)


### Deployment Pipeline

CI/CD for infrastructure and code deployment was setup using gitlab. The cloudformation templates gets validated and cfn_tag used to run security scan on the templates



### Requirements for Setting up
    - AWS Account
    - Gitlab Account


### Setting up
 
- Set up the following variables in Gitlab CI/CD Variable section
    - ACCESS_KEY_ID
    - SECRET_ACCESS_KEY
    - AWS_REGION

- Visit Gitlab, enter the necessary environment variables by clicking on CI/CD, then variables to enter the desired key value params
-  Run pipeline to initiate the build and test phase
-  Click on deploy button to create application stack








